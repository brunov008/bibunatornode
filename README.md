# bibunator

-- Em desenvolvimento -- 

it's three games, dude!

Projeto Web utilizando as ferramentas Node com framework express, React.JS, babel(para producao), Docker, linguagem Typescript/Ecmascript e Banco de dados Mysql versao 5.7

##Pre-requisitos:
Instalar docker

##Rodar aplicacao sem docker (caso queira criar o banco com o arquivo sql na propria maquina)
npm i && npm run dev

##Para configurar o banco mude o arquivo sql na pasta database-service e rode o comando: 
docker-compose up --build

##Rodar em modo desenvolvimento:
docker-compose up

##Rodar em modo desenvolvimento (em background):
docker-compose up -d

##Edite as variaveis na pasta .env se necessario (apenas em modo producao)

##Parar servidor
docker-compose down

##See postman collection
https://www.getpostman.com/collections/ec5d2b933d3faa57e208