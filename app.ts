import app from './src/back/config/express';

app.listen(Number(process.env.API_PORT), '0.0.0.0', () =>{
		console.log("Aplicação rodando na porta ",process.env.API_PORT)
  		console.log("host: "+process.env.HOST_NAME || 'localhost')
  		console.log("env: "+process.env.ENV_APP || 'development')
})