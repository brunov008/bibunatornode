import React, { Component } from 'react'
import {Container,Button} from 'react-bootstrap'
import {Inserir, Listar} from './index'

export class Perfil extends Component{

	constructor(props){
		super(props)

		this.state = {
			components:["inserir","listar"],
			render: null
		}
	}

	handleComponentChange = (component) => {
		switch(component){
			case 'inserir':
				this.setState({render:this.state.components[0]})
			break

			case 'listar':
				this.setState({render:this.state.components[1]})
			break

			default:
			break
		}
	}

	resolveComponent = () =>{
		let userToken

		if(this.props.location.state != null){
			userToken = this.props.location.state.token
		}else if(this.props.token != null){
			userToken = this.props.token
		}else{
			userToken = null
		}

		switch (this.state.render) {
			case "inserir":
				return <Inserir onPerfilComponentChange={this.handleComponentChange} tokenUsuario={userToken}/>
				break

			case "listar":
				return <Listar onPerfilComponentChange={this.handleComponentChange} tokenUsuario={userToken}/>
				break
			default:
				return <Inserir onPerfilComponentChange={this.handleComponentChange} tokenUsuario={userToken}/>
				break
		}
	}

	showName = () =>{
		if(this.props.location.state != null){
			return this.props.location.state.nome
		}else if(this.props.nome){
			return this.props.nome
		}else{
			return 'Usuario'
		}
	}

	render(){
		return(
			<Container className="d-flex flex-row">
				<Container className="d-flex flex-column col-2">
					<h3 className="mb-5">Olá {this.showName()}</h3>
					<Button className="mb-2" onClick={() => this.setState({render:this.state.components[0]})}>Inserir</Button>
					<Button className="mb-5" onClick={() => this.setState({render:this.state.components[1]})}>Listar</Button>
				</Container>
				<Container className="d-flex flex-column col-10">
					{this.resolveComponent()}
				</Container>
			</Container>
		);
	}
}