import React, { Component} from 'react';
import {Link,Redirect} from 'react-router-dom';
import {Button,Container,Row,Form,Col} from 'react-bootstrap';
import {Usuario} from '../../models';
import {Utility} from '../../util';

import {UsuarioService} from '../../services';

export class Login extends Component{

	constructor(props){
		super(props)
		this.state = {
			redirect : false,
			idusuario : null,
			nome : null,
			email : null,
			senha : null,
			token : null,
			emailValidation:false,
			senhaValidation:false
		}
	}

	handleChange = async event => {
		const { target } = event
		const value = target.value
		switch(target.id){
			case 'emailForm':
				if(Utility.validateEmail(value)){
					await this.setState({
						emailValidation:true, 
						email : value
					})
				}else{
					await this.setState({emailValidation:false})
				}
			break;

			case 'senhaForm':
				if(Utility.validatePassword(value)){
					await this.setState({
						senha:value,
						senhaValidation:true
					})
				}else{
					await this.setState({senhaValidation:false})
				}
			break
			default:
			break;
		}	
	}

	redirectToPerfil = () =>{
		return <Redirect push to={{
					pathname:"/perfil",
					state:this.state
				}}/>
	}

	/*
	showErrorMessage = ({variant, message}) => {
		//return <Alert variant={variant}>{message}</Alert> TODO
	}
	*/

	handleSubmit = async event => {
		event.preventDefault()

	    const form = event.currentTarget
	    if (form.checkValidity() === true) {

	    	const user = new Usuario()
	    	user.emailUsuario = this.state.email
	    	user.senhaUsuario = this.state.senha

	    	const result = await UsuarioService.logar(user)

	    	if(result.usuario != null && result.token != null){
	    		this.setState({
		    		redirect:true,
		    		idusuario:result.usuario.idusuario,
		    		email:result.usuario.emailUsuario,
		    		nome:result.usuario.nomeUsuario,
		    		token:result.token.accessToken
		    	})
	    	}else if(result.message != null){
	    		//{this.showErrorMessage({variant:"info", message:result.message})}
	    		alert(result.message)
	    	}
	    }
	  }

	render(){
		return(
			<Container>
				{(this.state.redirect)? this.redirectToPerfil():null}
				<Row className="justify-content-md-center mt-5">
					<Container className="w-auto p-5 shadow rounded">
						<Form noValidate 
						validated={this.state.emailValidation===true && this.state.senhaValidation===true} 
						onSubmit={this.handleSubmit}>
						  <Form.Group>
						    <Form.Label>Email</Form.Label>
						    <Form.Control 
						    id="emailForm" 
						    type="email" 
						    placeholder="Email"
						    isValid={this.state.emailValidation===true}
						    isInvalid={this.state.emailValidation===false}
						    onChange={this.handleChange} required/>
						    <Form.Control.Feedback>Email ok!</Form.Control.Feedback>
						    <Form.Control.Feedback type="invalid">Email inválido</Form.Control.Feedback>
						  </Form.Group>

						  <Form.Group>
						    <Form.Label>Senha</Form.Label>
						    <Form.Control 
						    id="senhaForm" 
						    type="password" 
						    placeholder="*****"
						    isValid={this.state.senhaValidation===true}
						    isInvalid={this.state.senhaValidation===false}
						    onChange={this.handleChange} required/>
						    <Form.Control.Feedback>Senha ok!</Form.Control.Feedback>
						    <Form.Control.Feedback type="invalid">Senha inválida</Form.Control.Feedback>
						  </Form.Group>
						  <Button variant="primary" type="submit">
						    Login
						  </Button>
						</Form>
						<hr></hr>
						<Row>
							<Col></Col>
							<Col md="auto">
								<label>Ainda não esta cadastrado?</label>
								<Link to="/usuario/cadastrar"> Cadastre-se</Link>
								<br></br>
								<label>Esqueci minha <Link to="/usuario/password-recover">senha</Link>?</label>
							</Col>
						</Row>
					</Container>
				</Row>
			</Container>
		);
	}
}