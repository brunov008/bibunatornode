import React,{Component} from 'react';
import {Redirect} from 'react-router-dom';
import {Container,Button,Form,Row} from 'react-bootstrap';
import {Utility} from '../../util';
import {Usuario} from '../../models';

import {UsuarioService} from '../../services';

export class Cadastrar extends Component{

	constructor(props){
		super(props)
		this.state = {
			redirect:false,
			emailValidation:false,
			nomeValidation:false,
			senhaValidation:false,
			email:null,
			nome:null,
			senha:null,
			token:null
		}
	}

	handleChange = async event => {
		const { target } = event
		const value = target.value
		switch(target.id){
			case 'emailCadastrarForm':
				if(Utility.validateEmail(value)){
					await this.setState({
						emailValidation:true, 
						email : value
					})
				}else{
					await this.setState({emailValidation:false})
				}
			break

			case 'nomeCadastrarForm':
				if(Utility.validateName(value)){
					await this.setState({
						nomeValidation:true, 
						nome : value
					})
				}else{
					await this.setState({nomeValidation:false})
				}
			break

			case 'senhaCadastrarForm':
				if(Utility.validatePassword(value)){
					await this.setState({
						senha:value,
						senhaValidation:true
					})
				}else{
					await this.setState({senhaValidation:false})
				}
			break
			default:
			break;
		}
	}

	redirectToHome = () =>{
		return <Redirect push to={{
					pathname:"/",
					state:this.state
				}}/>
	}

	checkValidation = () => {
		return (this.state.emailValidation
				&& this.state.nomeValidation
				&& this.state.senhaValidation)
	}

	handleSubmit = async event => {
		event.preventDefault()
		const form = event.currentTarget
		if (form.checkValidity() === true) {

			const user = new Usuario()
			user.emailUsuario = this.state.email
	    	user.senhaUsuario = this.state.senha
	    	user.nomeUsuario = this.state.nome

			const result = await UsuarioService.cadastrar(user)

			if(result.status != null && result.usuario != null && result.token != null){
				alert(result.message)
				this.setState({
					redirect:true,
					token:result.token.accessToken,
					nome : result.usuario.nomeUsuario,
					email : result.usuario.emailUsuario
				})
			}else{
				alert(result.message)
			}
		}
	}

	render(){
		return(
			<Container>
				{(this.state.redirect)? this.redirectToHome():null}
				<Row className="justify-content-md-center mt-5">
					<Container className="w-auto p-5 shadow rounded">
						<Form
							noValidate 
							validated={this.checkValidation}
							onSubmit={this.handleSubmit}>
							<Form.Group>
								<Form.Label>Email</Form.Label>
								<Form.Control
									id="emailCadastrarForm" 
						    		type="email" 
						    		placeholder="Email"
								    isValid={this.state.emailValidation===true}
								    isInvalid={this.state.emailValidation===false}
								    onChange={this.handleChange} 
								    required/> 
								<Form.Control.Feedback>Email ok!</Form.Control.Feedback>
								<Form.Control.Feedback type="invalid">Email inválido</Form.Control.Feedback>
							</Form.Group>
							<Form.Group>
								<Form.Label>Nome</Form.Label>
								<Form.Control
									id="nomeCadastrarForm" 
						    		type="text" 
						    		placeholder="Nome"
								    isValid={this.state.nomeValidation===true}
								    isInvalid={this.state.nomeValidation===false}
								    onChange={this.handleChange} 
								    required/>  
								<Form.Control.Feedback>Nome ok!</Form.Control.Feedback>
								<Form.Control.Feedback type="invalid">Nome inválido</Form.Control.Feedback>
							</Form.Group>
							<Form.Group>
								<Form.Label>Senha</Form.Label>
								<Form.Control
									id="senhaCadastrarForm" 
						    		type="password" 
						    		placeholder="*****"
								    isValid={this.state.senhaValidation===true}
								    isInvalid={this.state.senhaValidation===false}
								    onChange={this.handleChange} 
								    required/>
								<Form.Control.Feedback>Senha ok!</Form.Control.Feedback>
								<Form.Control.Feedback type="invalid">Senha inválida</Form.Control.Feedback>
							</Form.Group>
							<Button variant="primary" type="submit">
							    Cadastrar
							</Button>
						</Form>
					</Container>
				</Row>
			</Container>
		)
	}
}