import React, { Component} from 'react';
import {Container,Card,Button} from 'react-bootstrap'

import {DrunknatorService} from '../../services'

export class DrunkNator extends Component{

	constructor(props){
		super(props)

		this.state = {
			card: null
		}
	}

	componentDidMount(){
		this.requestObject()
	}

	requestObject = async () => {
		let result = await DrunknatorService.getRandomCarta()
		if(result){
			this.setState({ card:result })
		}
	}

	showLoading = () =>{
		return (<div>Carregando...</div>)
	}

	renderCards = () =>{
		const card = this.state.card
		return <Container className="d-flex flex-column">
					<Card bg="light" border="info" style={{ width: '18rem' }}>
					    <Card.Img variant="top" src={'http://'+card.imagem} />
					    <Card.Body>
					      <Card.Title>{card.titulo}</Card.Title>
					      <Card.Text>
					        {card.descricao}
					      </Card.Text>
					    </Card.Body>
					</Card>
					<Button onClick={() => this.requestObject()}>Próximo</Button>
				</Container>
	}

	render(){

	    return (
			<Container>
			  {(this.state.card == null) ? this.showLoading() : this.renderCards()}
			</Container>
    	)
	}
}