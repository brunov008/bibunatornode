import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App';
import {Login,Cadastrar,PasswordRecover, Perfil} from './components';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Route } from 'react-router-dom'

ReactDOM.render(
	<BrowserRouter>
		<Route path="/" exact={true} component={App} />
		<Route path="/usuario/login" component={Login} />
		<Route path="/usuario/password-recover" component={PasswordRecover} />
		<Route path="/usuario/cadastrar" component={Cadastrar} />
		<Route path="/perfil" component={Perfil} />
	</ BrowserRouter>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
