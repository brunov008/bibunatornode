export class Carta{

	constructor(cartaTipo, titulo, descricao, imagem){
		this.cartaTipo = cartaTipo
		this.titulo = titulo
		this.descricao = descricao
		this.imagem = imagem
	}

	get cartaTipo(){
		return this.cartaTipo
	}

	get titulo(){
		return this.titulo
	}

	get descricao(){
		return this.descricao
	}

	get imagem(){
		return this.imagem
	}
}