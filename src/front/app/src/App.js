import React, {Component} from 'react';
import {DrunkNator,Home} from './components';
import {Container,Alert} from 'react-bootstrap';
import {CustomNavbar} from './components';

export default class App extends Component{

	static DRUNKNATOR = "drunknator"
	static APP2 = "app2"
	static APP3 = "app3"
	static HOME = "home"

	constructor(props){
		super(props)

		this.state = {
			changeComponent:false,
			component:null
		}
	}

	handleComponentChange = component => {
		this.setState({
			changeComponent : true,
			component:component
		})
	}

	passUserData = () => {
		if(this.props.location.state != null){
			return {
				token: this.props.location.state.token,
				email: this.props.location.state.email,
				nome: this.props.location.state.nome
			}
		}
	}

	render(){
		let action;

		if(this.state.changeComponent){
			switch(this.state.component){
				case App.DRUNKNATOR: 
					action = <DrunkNator/>
					break;

				case App.APP2:
					action = <Alert key="alertKey" variant="warning">Falta implementar app2</Alert>
					break

				case App.APP3:
					action = <Alert key="alertKey" variant="warning">Falta implementar app3</Alert>
					break

				case App.HOME:
					action = <Home/>
					break
				default:
					action = alert("Error")
					break
			}
		}else{
			action = <Home/>
		}

	    return (
	    	<Container fluid="true" className="m-0 p-0">
		    	<CustomNavbar onComponentChange={this.handleComponentChange} user={this.passUserData()} className="m-0 p-0"/>
				<Container fluid={true} className="m-0 p-0">
					{action}
				</Container>
			</Container>
    	)
	} 
} 
