const jwt = require('jwt-simple');
const moment = require('moment-timezone');
import {Request, Response} from 'express';
import vars from '../../config/vars';
import Usuario from '../routes/usuario/models/usuario';

class Token{
    constructor(){

    }

    static tokenMiddleware = (req:Request, res:Response, next:any) => {
        const token = req.body.token || req.query.token || req.headers['x-access-token']
        if (token) {
                let decoded = null;
                try{
                    decoded = jwt.decode(token, vars.jwtSecret)
                }catch(e){
                    return res.status(403).send({
                        message: 'Token invalido'
                    })
                }
                if (!decoded) {
                    return res.status(403).send({
                        message: 'Token invalido'
                    })
                } else {
                    (req as any).decoded = decoded.sub
                    next()
                }
        } else {
            return res.status(403).send({
                message: 'Token invalido'
            })
        }
    }

    static  generateTokenResponse = (user:Usuario, accessToken:string) => {
        const tokenType = 'Bearer'
        // const refreshToken = RefreshToken.generate(user).token; TODO
        const expiresIn = moment().add(525600, 'minutes')
        return {tokenType, accessToken, expiresIn}
    }

    static findAndGenerateToken = async(body:any) => {
        const {email, senha, nome} = body

        if(!email) throw Error('Requer um e-mail para gerar um token')
        if(!senha) throw Error('Requer uma senha para gerar um token')
        
        const usuario = await Usuario.getByEmail(email)

        if(usuario != null || usuario != undefined){
            if(usuario.senhaUsuario == senha){
                return {usuario: usuario, accessToken: Token.token(usuario)}
            }
        }
        throw Error('Usuario não existe')

    }

    static token(user:Usuario) {
        const payload = {
            exp: moment().add(525600, 'minutes').unix(),
            iat: moment().unix(),
            sub: user
        }

        return jwt.encode(payload, vars.jwtSecret)
    }
}
export default Token;
