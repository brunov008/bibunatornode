import {Request,Response} from 'express'
import Cartas from './models/cartas'
import TipoCarta from './models/tipocarta'
import Usuario from '../usuario/models/usuario'

class DrunkController{

  constructor(){
  }

  create = async (req:Request, res:Response) => {
    try{ 

      const usuarioToken = (req as any).decoded //usuario do token

      const {cartaTipo, titulo, descricao} = req.body

      if(!cartaTipo || !titulo || !descricao || !(req as any).files.image) return res.json('Parametros esperados')

      const image = (req as any).files.image

      const tmp = image.path
      const target = require('path').resolve(__dirname, '..', '..', '..', '..', '..') 
      + '/public/'
      + usuarioToken.emailUsuario
      + '/' 
      + image.name

      require('file-system').copyFile(tmp, target, {
        done: async (err:any) => {
          if(err) return res.json({message:err})
          
          const tipoCarta = new TipoCarta(null, cartaTipo, usuarioToken.idusuario)
          const tipo:TipoCarta = await tipoCarta.create()

          if(tipo){
            const imagePath = this.imagePath(usuarioToken.emailUsuario, image.name)

            const carta = new Cartas(null,titulo,descricao,imagePath,tipo.idTipoCarta)
            const cartaResult:Cartas = await carta.create() 

            if (cartaResult){
              return res.json({message: "Carta inserida com sucesso", carta:cartaResult})
            }
          }
        }
      })
    }catch(e){
      return res.json({message:e.message}) 
    }
  }

  imagePath = (email:any, imageName:any) => {
    let host = process.env.HOST_NAME || 'localhost'
    return host
            + ':' 
            + process.env.API_PORT
            + '/' 
            + email 
            + '/' 
            + imageName
  }

  getAllCartas = async(req:Request, res:Response) => {
    try{
      const {tipo} = req.query || req.params
      let result = null

      if(tipo){
        result = await Cartas.getCartasByTipo(tipo)
      }else{
        result = await Cartas.getAll()
      }

      return res.json({result:result})
    }catch(e){
      return res.json({message:e.message}) 
    }
  }

  listarTipos = async (req:Request, res:Response) => {
    try{
      const usuarioToken = (req as any).decoded

      const result = await TipoCarta.listarTipos()

      let responseBody = null

      if(usuarioToken.role === 'admin'){
        responseBody = {tipos:result, admin:true}
      }else{
        responseBody = {tipos:result, admin:false}
      }

      return res.json(responseBody)
    }catch(e){
      return res.json({message:e.message}) 
    }
  }

  criarTipo = async (req:Request, res:Response) => {
    try{
      const usuarioToken = (req as any).decoded

      const {tipo} = req.body

      if(!tipo) return res.json('Parametros esperados')

       const tipoCarta = new TipoCarta(null, tipo, usuarioToken.idusuario)
       const tipoResult:TipoCarta = await tipoCarta.create()

      if(tipoResult){
        return res.json({message: "Tipo criado com sucesso", tipo:tipoResult})
      }
    }catch(e){
      return res.json({message:e.message}) 
    }
  }

  update = async (req:Request, res:Response) => {
    try{ 
      
    }catch(e){
      res.json({message:e.message})
    }
  }

  delete = async (req:Request, res:Response) => {
    try{ 
      
    }catch(e){
      return res.json({message:e.message}) 
    }
  }

  getRandom = async (req:Request, res:Response) => {
    try{ 
      const result = await Cartas.getCarta()

      if(result){
          return res.json({name: "Nome", descricao:result.descricao, titulo: result.titulo, imagem:result.imagem})
      }

      return res.json({})
    }catch(e){
      return res.json({message:e.message}) 
    }
  }
}

export default new DrunkController;