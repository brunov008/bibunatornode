import DrunkController from './drunkcontroller';
import Token from '../../middlewares/token';

const drunkroute = require('express').Router();
const multipart = require('connect-multiparty');
const multipartMiddleware = multipart();

drunkroute.get('/carta',DrunkController.getRandom)
drunkroute.get('/cartas',DrunkController.getAllCartas)
drunkroute.get('/cartas/:tipo',DrunkController.getAllCartas)
drunkroute.post('/cartas/create',Token.tokenMiddleware,multipartMiddleware,DrunkController.create)
drunkroute.get('/tipos',Token.tokenMiddleware,DrunkController.listarTipos)
drunkroute.put('/tipos/create',Token.tokenMiddleware,DrunkController.criarTipo)

module.exports = drunkroute;