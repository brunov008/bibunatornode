const BD = require('../../../../config/bd');

export default class TipoCarta{

	public idTipoCarta?:number;
	public nomeTipo?:string;
	public idusuario?:number;

	constructor(idTipoCarta?:any, nomeTipo?:string, idusuario?:number){
		this.idTipoCarta = idTipoCarta
		this.nomeTipo = nomeTipo
		this.idusuario = idusuario
	}

	create = async () => {
		const sql = 'insert into bibunator.TipoCartas (nomeTipo, usuario_idusuario) values (?,?);'

		const result = await BD.query(sql,[this.nomeTipo, this.idusuario])

		let tipoCarta = await TipoCarta.getTipoCartaByNome(this.nomeTipo!!)

		return tipoCarta
	}

	static listarTipos = async () => {
		const sql = 'select DISTINCT (nomeTipo) from bibunator.TipoCartas;'

		const result = await BD.query(sql)

		let list = result.map((value:any) => {
			return value.nomeTipo
		})

		return list
	}

	static getTipoCartaByNome = async(nomeTipo:string) => {
		const sql = 'select * from bibunator.TipoCartas where nomeTipo = ?;'

		const result = await BD.query(sql,[nomeTipo])

		let tipoCartaResult = new TipoCarta(result[0].idTipoCarta, result[0].nomeTipo, result[0].usuario_idusuario)

		return tipoCartaResult
	}
}