let BD = require('../../../../config/bd')

export default class Cartas{

	private idCarta?:number;
	private titulo?:string;
	private descricao?:string;
	private imagem?:string
	private TipoCartas_idTipo?:number;

	constructor(idCarta?:any, titulo?:string, descricao?:string, imagem?:string, TipoCartas_idTipo?: number){
		this.idCarta = idCarta
		this.titulo = titulo
		this.descricao = descricao
		this.imagem = imagem
		this.TipoCartas_idTipo = TipoCartas_idTipo
	}

	create = async () => {
		let sql = 'insert into bibunator.CartasDrunkNator (titulo, descricao, imagem, TipoCartas_idTipo) values (?,?,?,?);'

		let result = await BD.query(sql,[this.titulo, this.descricao, this.imagem, this.TipoCartas_idTipo])

		let carta = await Cartas.getCartaByTituloAndDescricao(this.titulo!!, this.descricao!!)

		return carta
	}

	static getAll = async() => {
		let sql = 'SELECT * FROM bibunator.CartasDrunkNator;'

		let result = await BD.query(sql)

		return result
	}

	static getCartasByTipo = async (tipo:string) =>{
		let sql = 'select * from bibunator.CartasDrunkNator as a inner join bibunator.TipoCartas as b on a.TipoCartas_idTipo = b.idTipoCarta where b.nomeTipo = ?;'

		let result = await BD.query(sql,[tipo])

		return result
	}

	static getCartaByTituloAndDescricao = async (titulo:string, descricao:string) =>{
		let sql = 'SELECT * FROM bibunator.CartasDrunkNator where titulo=? and descricao=?;'

		let result = await BD.query(sql,[titulo, descricao]) //retorna lista

		let cartaResult = new Cartas(result[0].idCarta, result[0].titulo, result[0].descricao,result[0].imagem,result[0].TipoCartas_idTipo)

		return cartaResult
	}

	/*
	static getCartasUmTipo = async (idTipo:number) => {
		let sql = 'SELECT * FROM bibunator.CartasDrunkNator WHERE TipoCartas_idTipo = ? ORDER BY RAND() LIMIT 1;'

		let result = await BD.query(sql,[idTipo])

		return result
	}
	*/

	static getCarta = async () => {
		let sql = 'SELECT * FROM bibunator.CartasDrunkNator ORDER BY RAND() LIMIT 1;'

		let result = await BD.query(sql)

		return result[0]
	}
}