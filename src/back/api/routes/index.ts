const router = require('express').Router(); 

const drunknator = require('./drunknator');
const usuario = require('./usuario');
//const app2 = require('./app2');
//const app3 = require('./app3');

router.use('/drunknator', drunknator)
router.use('/usuario', usuario)
//router.use('/app2', app2)
//router.use('/app3', app3)

module.exports = router;