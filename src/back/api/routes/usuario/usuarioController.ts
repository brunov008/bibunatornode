import {Request, Response} from 'express';
import Usuario from './models/usuario';
import Token from '../../middlewares/token';

class UsuarioController{

	 constructor(){
		
	 }

	 authenticate = async (req:Request, res:Response) => {
	 	try{
		 	const {usuario, accessToken} = await Token.findAndGenerateToken(req.body)
		 	const token = Token.generateTokenResponse(usuario, accessToken)
		 	return res.json({token, usuario: usuario})
	 	}catch(e){
	 		return res.json({message:e.message}) 
	 	}
	 }

	create = async (req:Request, res:Response, next:any) =>{
		try{
			const {nome, email, senha, role} = req.body

			if(nome == null || email == null || senha == null) return res.json('Parametros esperados')

			let user

			if(role != null){
				user = new Usuario(nome, email, senha, role)
			}else{
				user = new Usuario(nome, email, senha)
			}

			const result = await user.create()

			const {usuario, accessToken} = await Token.findAndGenerateToken(user)
			const token = Token.generateTokenResponse(usuario, accessToken)

			return res.status(200).json({
				status:true,
				message:'Usuario criado com sucesso',
				usuario: usuario,
				token
			})
		}catch(e){
			return res.json({message:e.message}) 
		}
	}

	update = async (req:Request, res:Response) =>{
		try{
			//TODO
		}catch(e){
			return res.json({message:e.message}) 
		}
	}

	delete = async (req:Request, res:Response) =>{
		try{
			//TODO
		}catch(e){
			return res.json({message:e.message}) 
		}
	}

	get = async (req:Request, res:Response) =>{
		try{
			//TODO
		}catch(e){
			return res.json({message:e.message}) 
		}
	}
}

export default new UsuarioController;