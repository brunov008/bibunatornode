const BD = require('../../../../config/bd')

export default class Usuario{

	private nome:string
	private email:string
	private senha:string
	private role?:string

	constructor(nome:string, email:string, senha:string, role?:string){
		this.nome = nome;
		this.email = email
		this.senha = senha
		this.role = role
	}

	create = async () => {
		let sql
		let result

		if(this.role != null){
			sql = 'insert into bibunator.Usuario (nomeUsuario, emailUsuario, senhaUsuario, role) values (?,?,?,?)'
			result = await BD.query(sql,[this.nome, this.email,this.senha,this.role!!])
		}else{
			sql = 'insert into bibunator.Usuario (nomeUsuario, emailUsuario, senhaUsuario) values (?,?,?)'
			result = await BD.query(sql,[this.nome, this.email,this.senha])
		}

		return result
	}

	static getById = async (id:number) => {
		const sql = 'select * from bibunator.Usuario where idusuario=?'

		const result = await BD.query(sql,[id])

		return result
	}

	static getByEmail = async (email:string) => {
		const sql = 'select * from bibunator.Usuario where emailUsuario=?'

		const result = await BD.query(sql,[email])

		return result[0]
	}
}