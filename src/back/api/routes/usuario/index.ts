const usuarioroute = require('express').Router()

import UsuarioController from './usuarioController'
import Token from '../../middlewares/token'

usuarioroute.put('/create', UsuarioController.create)
usuarioroute.get('/get', UsuarioController.get)
usuarioroute.delete('/:id', UsuarioController.delete)
usuarioroute.post('/update',UsuarioController.update)
usuarioroute.post('/authenticate', UsuarioController.authenticate)

module.exports = usuarioroute