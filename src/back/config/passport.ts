import vars from './vars'
import Usuario from '../api/routes/usuario/models/usuario'

const JwtStrategy = require('passport-jwt').Strategy
const { ExtractJwt } = require('passport-jwt')

const jwtOptions = {
    secretOrKey: vars.jwtSecret,
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('Bearer')
}

const jwt = async (payload:any, done:any) => {
    try {
        const user = await Usuario.getById(payload.sub)
        if (user) return done(null, user)
        return done(null, false)
    } catch (error) {
        return done(error, false)
    }
}

exports.jwt = new JwtStrategy(jwtOptions, jwt);