class GerenciadorDB{

	constructor(){
		this._connection = require('mysql').createConnection({
		  	host: process.env.MYSQL_HOST || 'localhost',
		  	user: 'root',
		  	password: process.env.MYSQL_ROOT_PASSWORD || '',
		  	database: process.env.MYSQL_DATABASE || 'bibunator',
            port: '3306'
		})

        this._connection.connect((err) => {
            if(err){
                console.log("Ocorreu um erro ao se conecatar com o banco de dados. Veja se o serviço esta rodando ou as crendeciais estao certas")
                return null
            }
            console.log('conexao com banco com sucesso')
            return this.connection
        })
	}

	query( sql, args ) {
        return new Promise( ( resolve, reject ) => {
            this._connection.query( sql, args, ( err, rows ) => {
                if ( err ) return reject( err )
                resolve( rows )
            })
        })
    }
    
    close() {
        return new Promise( ( resolve, reject ) => {
            this._connection.end( (err) => {
                if ( err ) return reject( err )
                resolve() 
            })
        })
    }
}

module.exports = new GerenciadorDB();