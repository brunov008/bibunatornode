import express from 'express'
import bodyParser from 'body-parser'

const app = express()
const cors = require('cors')
const routes = require('../api/routes')
const path = require ('path')
const passport = require('passport')
const strategies = require('./passport')
const frontPath = path.resolve(__dirname, '..', '..', '..') + '/src/front'

if (process.env.NODE_ENV !== 'production'){
	 require('dotenv').config() //load the .env variables
}

const publicPath = process.env.PWD + '/public'

app.use(cors())
app.use(bodyParser.json())
app.use(express.static(publicPath))
app.use(passport.initialize())
passport.use('jwt', strategies.jwt)

//Rotas
app.use('/api', routes)

export default app;