FROM node:latest

ADD . /code
WORKDIR /code

RUN npm i
CMD chmod 777 wait-for-it.sh
CMD ./wait-for-it.sh db:3306 -- npm run server
