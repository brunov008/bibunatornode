-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema bibunator
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema bibunator
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `bibunator` DEFAULT CHARACTER SET utf8 ;
USE `bibunator` ;

-- -----------------------------------------------------
-- Table `bibunator`.`Usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bibunator`.`Usuario` (
  `idusuario` INT NOT NULL AUTO_INCREMENT,
  `nomeUsuario` VARCHAR(45) NOT NULL,
  `emailUsuario` VARCHAR(100) NOT NULL,
  `senhaUsuario` VARCHAR(45) NOT NULL,
  `role` ENUM('user', 'admin') DEFAULT 'user',

  PRIMARY KEY (`idusuario`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bibunator`.`TipoCartas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bibunator`.`TipoCartas` (
  `idTipoCarta` INT NOT NULL AUTO_INCREMENT,
  `nomeTipo` VARCHAR(45) NOT NULL,
  `usuario_idusuario` INT NOT NULL,
  PRIMARY KEY (`idTipoCarta`),
  INDEX `fk_TipoCartas_usuario1_idx` (`usuario_idusuario` ASC),
  CONSTRAINT `fk_TipoCartas_usuario1`
    FOREIGN KEY (`usuario_idusuario`)
    REFERENCES `bibunator`.`Usuario` (`idusuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bibunator`.`CartasDrunkNator`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bibunator`.`CartasDrunkNator` (
  `idCarta` INT NOT NULL AUTO_INCREMENT,
  `titulo` VARCHAR(45) NOT NULL,
  `descricao` VARCHAR(300) NOT NULL,
  `imagem` VARCHAR(300) NOT NULL,
  `TipoCartas_idTipo` INT NOT NULL,
  PRIMARY KEY (`idCarta`),
  INDEX `fk_CartasDrunkNator_TipoCartas_idx` (`TipoCartas_idTipo` ASC),
  CONSTRAINT `fk_CartasDrunkNator_TipoCartas`
    FOREIGN KEY (`TipoCartas_idTipo`)
    REFERENCES `bibunator`.`TipoCartas` (`idTipoCarta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bibunator`.`TipoPergunta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bibunator`.`TipoPergunta` (
  `idTipoPergunta` INT NOT NULL AUTO_INCREMENT,
  `nomeTipo` VARCHAR(45) NOT NULL,
  `usuario_idusuario` INT NOT NULL,
  PRIMARY KEY (`idTipoPergunta`),
  INDEX `fk_TipoPergunta_usuario1_idx` (`usuario_idusuario` ASC),
  CONSTRAINT `fk_TipoPergunta_usuario1`
    FOREIGN KEY (`usuario_idusuario`)
    REFERENCES `bibunator`.`Usuario` (`idusuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bibunator`.`PerguntaAdivinhaNator`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bibunator`.`PerguntaAdivinhaNator` (
  `idAdivinhaNator` INT NOT NULL AUTO_INCREMENT,
  `pergunta` VARCHAR(300) NOT NULL,
  `TipoPergunta_idTipoPergunta` INT NOT NULL,
  PRIMARY KEY (`idAdivinhaNator`),
  INDEX `fk_PerguntaAdivinhaNator_TipoPergunta1_idx` (`TipoPergunta_idTipoPergunta` ASC),
  CONSTRAINT `fk_PerguntaAdivinhaNator_TipoPergunta1`
    FOREIGN KEY (`TipoPergunta_idTipoPergunta`)
    REFERENCES `bibunator`.`TipoPergunta` (`idTipoPergunta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bibunator`.`tipoQuizzNator`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bibunator`.`tipoQuizzNator` (
  `idtipoQuizzNator` INT NOT NULL AUTO_INCREMENT,
  `nomeTipo` VARCHAR(45) NOT NULL,
  `Usuario_idusuario` INT NOT NULL,
  PRIMARY KEY (`idtipoQuizzNator`),
  INDEX `fk_tipoQuizzNator_Usuario1_idx` (`Usuario_idusuario` ASC),
  CONSTRAINT `fk_tipoQuizzNator_Usuario1`
    FOREIGN KEY (`Usuario_idusuario`)
    REFERENCES `bibunator`.`Usuario` (`idusuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bibunator`.`respostas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bibunator`.`respostas` (
  `idrespostas` INT NOT NULL AUTO_INCREMENT,
  `resposta` VARCHAR(45) NOT NULL,
  `PerguntaQuizzNator_idPerguntaQuizzNator` INT NOT NULL,
  PRIMARY KEY (`idrespostas`),
  INDEX `fk_respostas_PerguntaQuizzNator1_idx` (`PerguntaQuizzNator_idPerguntaQuizzNator` ASC),
  CONSTRAINT `fk_respostas_PerguntaQuizzNator1`
    FOREIGN KEY (`PerguntaQuizzNator_idPerguntaQuizzNator`)
    REFERENCES `bibunator`.`PerguntaQuizzNator` (`idPerguntaQuizzNator`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bibunator`.`PerguntaQuizzNator`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bibunator`.`PerguntaQuizzNator` (
  `idPerguntaQuizzNator` INT NOT NULL AUTO_INCREMENT,
  `pergunta` VARCHAR(45) NOT NULL,
  `tipoQuizzNator_idtipoQuizzNator` INT NOT NULL,
  `respostas_idrespostas` INT NOT NULL,
  PRIMARY KEY (`idPerguntaQuizzNator`),
  INDEX `fk_PerguntaQuizzNator_tipoQuizzNator1_idx` (`tipoQuizzNator_idtipoQuizzNator` ASC),
  INDEX `fk_PerguntaQuizzNator_respostas1_idx` (`respostas_idrespostas` ASC),
  CONSTRAINT `fk_PerguntaQuizzNator_tipoQuizzNator1`
    FOREIGN KEY (`tipoQuizzNator_idtipoQuizzNator`)
    REFERENCES `bibunator`.`tipoQuizzNator` (`idtipoQuizzNator`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_PerguntaQuizzNator_respostas1`
    FOREIGN KEY (`respostas_idrespostas`)
    REFERENCES `bibunator`.`respostas` (`idrespostas`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
